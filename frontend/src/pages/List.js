import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';



function List(){

let[users, setUsers] = useState([]);

async function getData(){

    var resp = await fetch("http://localhost:2000/user/sel");
    var data = await resp.json();
    setUsers(data);
    console.log(data);
}

useEffect(()=>{
    getData();
},[])

    return(
        <>

            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Gender</th>
                    <th>Country</th>
                    <th>Stream</th>
                    <th>Image</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                {
                    users.map((u)=>
                    <tr key={u._id}>
                        <td>{u.name}</td>
                        <td>{u.email}</td>
                        <td>{u.gender}</td>
                        <td>{u.country}</td>
                        <td>{u.stream}</td>
                        <td><img src={"http://localhost:2000/upimg/"+ u.pimg} style={ { height: 200 }}/></td>
                        <td><button className="btn btn-danger" onClick={async()=>{

                            if(window.confirm("Are you sure?")){

                                var fd = new FormData();
                                fd.append("id",u._id);
                                var resp = await fetch("http://localhost:2000/user/del",{
                                    method:'POST',
                                    body:fd,
                                });

                                var data = await resp.json();
                                console.log(data);

                                getData();
                            }

                            

                        }}>Delete</button></td>
                        <td><Link to={"/edituser/"+u._id} className="btn btn-primary">Edit</Link></td>
                    </tr>

                    )
                }
              
                
                </tbody>
            </table>

            
        </>
    )
}

export default List;