import {useState} from 'react';


function Login(){


let [email,setEmail]= useState('');
let [password,setPass]= useState('');
let [lerr,setLerr]=useState(false);


    return(
        <>
        {lerr?<div className="alert alert-danger alert-dismissible fade show"><button type="button" className="close" data-dismiss="alert">&times;</button><strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.</div>:" "}
        <div className="formdiv">
        <h2 className="text-center">Registration From</h2>
        <div className="formstyle">
           
            <p>Email:</p>
            <p><input type="email" name="email" onChange={(ev)=>{
                setEmail(ev.target.value);
            }}/></p>
            <p>Password:</p>
            <p><input type="password" name="password" onChange={(ev)=>{
                setPass(ev.target.value);
            }}/></p>
           
          
            <p><input type="button" value="Submit" className="btn btn-primary" onClick={async ()=>{

                var fd = new FormData();
              
                fd.append('email',email);
                fd.append('password',password)

                var resp = await fetch("http://localhost:2000/auth/login",{
                    method:'POST',
                    body: fd,

                });

                var data= await resp.json();
                console.log(data);
                
                if(data.msg=="invalid login"){
                    setLerr(true)
                }else{
                    localStorage.setItem("token",data.token);
                    // setLerr(false);
                    // localStorage.setItem("uid",data._id);
                    // localStorage.setItem("uname",data.name);
                    // window.location = "/";
                }

             }}/></p> 
        </div>
        </div>
       
        </>
    )
}

export default Login;