import {useState, useEffect} from 'react';


function Edituser(props){

let [name,setName]=useState('');
let [email,setEmail]= useState('');
let [gender,setGender]=useState('');
let [isMale,setIsmale]=useState(false);
let [isFemale,setIsfemale]=useState(false);
let [country,setCountry]=useState('');
let [stream,setStream]=useState([]);
let [isCse,setIscse]=useState(false);
let [isEce,setIsece]=useState(false);
let [isIt,setIsit]=useState(false);
let [pimg, setPimg]=useState(null);
let [imgUrl, setImgurl]=useState('')

function sfun(ev){

   
    if(ev.target.checked == true){
        stream.push(ev.target.value)
        if(ev.target.value == "CSE"){
            setIscse(true);
        }
        if(ev.target.value == "ECE"){
            setIsece(true);
        }
        if(ev.target.value == "IT"){
            setIsit(true);
        }
    }
    else{

        var ind = stream.indexOf(ev.target.value);
        stream.splice(ind,1);
        if(ev.target.value == "CSE"){
            setIscse(false);
        }
        if(ev.target.value == "ECE"){
            setIsece(false);
        }
        if(ev.target.value == "IT"){
            setIsit(false);
        }


    }

    console.log(stream);
}


async function getEdit(){

    var id= props.match.params.id;
    var fd = new FormData();
    fd.append("id",id);
    var resp = await fetch("http://localhost:2000/user/edit",{
        method:'POST',
        body:fd
    });

    var data = await resp.json();
    setName(data.name);
    setEmail(data.email);
    if(data.gender == 'Male')
    {
        setIsmale(true);
    }
    else{
        setIsfemale(true);
    }
    setCountry(data.country);
    stream=data.stream.split(",");
    setStream(stream);

    if(stream.indexOf("CSE")!="-1")
    {
        setIscse(true);
    }
    if(stream.indexOf("ECE")!="-1"){
        setIsece(true);
    }
    if(stream.indexOf("IT")!="-1"){
        setIsit(true);
    }
    setImgurl(data.pimg);
}

useEffect(()=>{
    
   getEdit();
},[])


    return(
        <>
        <div className="formdiv">
        <h2 className="text-center">Registration From</h2>
        <div className="formstyle">
            <p>Name</p>
            <p><input type="text" value={name} name="name" onChange ={(ev)=>{
                setName(ev.target.value);
            }}
            /></p>
            <p>Email:</p>
            <p><input type="email" value={email} name="email" onChange={(ev)=>{
                setEmail(ev.target.value);
            }}/></p>
            <p>Gender:</p>
            <p><input type="radio" checked={isMale} name="gender" value="Male" onChange={(ev)=>{
                setGender(ev.target.value);
                setIsmale(true);
                setIsfemale(false);
            }}/> Male</p>
            <p><input type="radio" checked={isFemale} name="gender" value="Female" onChange={(ev)=>{
                setGender(ev.target.value);
                setIsmale(false);
                setIsfemale(true);
            }}/> Female</p>
            <p>Country:</p>
            <p>  <select name="cars" value={country} onChange={(ev)=>{
                setCountry(ev.target.value)
            }}>
                        <option value="India">India</option>
                        <option value="USA">USA</option>
                        <option value="France">France</option>
                       
                    </select>
            </p>
            <p>Stream:</p>
            <p><input type="checkbox" checked={isCse} value="CSE" onChange={sfun}/>CSE</p>
            <p><input type="checkbox" checked={isEce} value="ECE" onChange={sfun}/>ECE</p>
            <p><input type="checkbox" checked={isIt} value="IT" onChange={sfun}/>IT</p>
            <p><input type="file" onChange={(ev)=>{
                setPimg(ev.target.files[0]);
            }}/></p>
            <p><img src={"http://localhost:2000/upimg/"+imgUrl} style={{height:200} }/></p>
            <p><input type="button" value="Submit" className="btn btn-primary" onClick={async ()=>{

                var fd = new FormData();
                fd.append('name',name);
                fd.append('email',email);
                fd.append('gender',gender);
                fd.append('country',country);
                fd.append('stream',stream);
                fd.append('pimg',pimg);
                fd.append("id", props.match.params.id);

                var resp = await fetch("http://localhost:2000/user/upd",{
                    method:'POST',
                    body: fd,

                });

                var data= await resp.json();
                console.log(data);

             }}/></p> 
        </div>
        </div>
       
        </>
    )
}

export default Edituser;