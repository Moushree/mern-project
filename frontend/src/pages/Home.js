import {useState} from 'react';

function Home(){

let [name,setName]=useState('');
let [email,setEmail]= useState('');
let [gender,setGender]=useState('');
let [country,setCountry]=useState('');
let [stream,setStream]=useState([]);
let [pimg, setPimg]=useState(null);

function sfun(ev){

    if(ev.target.checked == true){
      stream.push(ev.target.value)
    }
    else{
        let ind = stream.indexOf(ev.target.value);

        stream.splice(ind, 1)
    }

    console.log(stream);
}

    return(
        <>
        <div className="formdiv">
        <h2 className="text-center">Registration From</h2>
        <div className="formstyle">
            <p>Name</p>
            <p><input type="text" name="name" onChange ={(ev)=>{
                setName(ev.target.value);
            }}
            /></p>
            <p>Email:</p>
            <p><input type="email" name="email" onChange={(ev)=>{
                setEmail(ev.target.value);
            }}/></p>
            <p>Gender:</p>
            <p><input type="radio" name="gender" value="Male" onChange={(ev)=>{
                setGender(ev.target.value)
            }}/> Male</p>
            <p><input type="radio" name="gender" value="Female" onChange={(ev)=>{
                setGender(ev.target.value)
            }}/> Female</p>
            <p>Country:</p>
            <p>  <select name="cars" onChange={(ev)=>{
                setCountry(ev.target.value)
            }}>
                        <option value="India">India</option>
                        <option value="USA">USA</option>
                        <option value="France">France</option>
                       
                    </select>
            </p>
            <p>Stream:</p>
            <p><input type="checkbox" value="CSE" onChange={sfun}/>CSE</p>
            <p><input type="checkbox" value="ECE" onChange={sfun}/>ECE</p>
            <p><input type="checkbox" value="IT" onChange={sfun}/>IT</p>
            <p><input type="file" onChange={(ev)=>{
                setPimg(ev.target.files[0]);
            }}/></p>
            <p><input type="button" value="Submit" className="btn btn-primary" onClick={async ()=>{

                var fd = new FormData();
                fd.append('name',name);
                fd.append('email',email);
                fd.append('gender',gender);
                fd.append('country',country);
                fd.append('stream',stream);
                fd.append('pimg',pimg);

                var resp = await fetch("http://localhost:2000/user/ins",{
                    method:'POST',
                    body: fd,

                });

                var data= await resp.json();
                console.log(data);

             }}/></p> 
        </div>
        </div>
       
        </>
    )
}

export default Home;