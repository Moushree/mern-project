
import {useEffect} from 'react';

function About(){
    var token = localStorage.getItem("token");
    async function tokenVerify (){
        var resp = await fetch('http://localhost:2000/auth/protected',{
            headers:{
                // 'Content-Type': 'application/json',
                //  'X-Requested-With': 'XMLHttpRequest'
                'Authorization' : 'Bearer '+token
                
            }
            

        })

        var data = await resp.json();
        console.log(data);
    }

    useEffect(()=>{
        // let un = localStorage.getItem("uname");
        // if(un == null){
        //     window.location = '/login';
        // }
        tokenVerify();
    })

    return(
        <>
        <div className="formdiv">
        <h1>About Us</h1>
        </div>
        </>
    )
}

export default About;