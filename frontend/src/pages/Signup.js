import {useState} from 'react';


function Signup(){

let [name,setName]=useState('');
let [email,setEmail]= useState('');
let [password,setPass]= useState('');



    return(
        <>
        <div className="formdiv">
        <h2 className="text-center">Registration From</h2>
        <div className="formstyle">
            <p>Name</p>
            <p><input type="text" name="name" onChange ={(ev)=>{
                setName(ev.target.value);
            }}
            /></p>
            <p>Email:</p>
            <p><input type="email" name="email" onChange={(ev)=>{
                setEmail(ev.target.value);
            }}/></p>
            <p>Password:</p>
            <p><input type="password" name="password" onChange={(ev)=>{
                setPass(ev.target.value);
            }}/></p>
           
          
            <p><input type="button" value="Submit" className="btn btn-primary" onClick={async ()=>{

                var fd = new FormData();
                fd.append('name',name);
                fd.append('email',email);
                fd.append('password',password)

                var resp = await fetch("http://localhost:2000/auth/signup",{
                    method:'POST',
                    body: fd,

                });

                var data= await resp.json();
                console.log(data);

             }}/></p> 
        </div>
        </div>
       
        </>
    )
}

export default Signup;