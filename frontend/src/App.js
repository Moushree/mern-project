import { BrowserRouter, Switch, Route, Link} from 'react-router-dom';
import './App.css';
import { useState, useEffect } from 'react';
import Home from './pages/Home';
import About from './pages/About';
import List from './pages/List';
import Edituser from './pages/Edituser';
import Signup from './pages/Signup';
import Login from './pages/Login';
function App() {

let [islogin, setIslogin] = useState(false);
let [name, setName] = useState("");

useEffect(()=>{
  // let un = localStorage.getItem("uname");
  let un = localStorage.getItem("token");
  if(un!= null){
    // setName(un);
    setIslogin(true)
  }
  else{
    setIslogin(false);
  }

})


  return (
    <BrowserRouter>

      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand" to="#">Navbar</Link>
       

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link className="nav-link" to="/">Home <span className="sr-only">(current)</span></Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/about">About</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/list">List</Link>
            </li>
            {!islogin?<li className="nav-item"><Link className="nav-link" to="/signup">Signup</Link></li>:''}
            {!islogin?<li className="nav-item"><Link className="nav-link" to="/login">Login</Link></li>:''}
            {islogin?<li className="nav-item"><Link className="nav-link" to="">Welcome {name} </Link></li>:''}
            {islogin?<li className="nav-item"><button className="btn btn-danger" onClick={()=>{
              localStorage.removeItem("uid");
              localStorage.removeItem("uname");
              localStorage.removeItem("token");
              window.location = "/";
            }}>Logout</button></li>:''}
          
          </ul>
         
        </div>
      </nav>

      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/about" component={About}/>
        <Route exact path="/list" component={List}/>
        <Route exact path="/edituser/:id" component={Edituser}/>
        <Route exact path="/signup" component={Signup}/>
        <Route exact path="/login" component={Login}/>
      </Switch>
    </BrowserRouter>

  );
}

export default App;
