const express = require("express");

const router = express.Router();

router.get("/sel",(req,res)=>{
    res.send("I am Selected")
})

router.get("/ins",(req,res)=>{
    res.send("I am Inserted")
})

router.get("/edit",(req,res)=>{
    res.send("I am updated")
})

router.get("/del",(req,res)=>{
    res.send("I am deleted")
})

module.exports = router;