const express = require("express");
const jwt = require('jsonwebtoken');
const router = express.Router();
const bcrypt = require("bcrypt");
//var dbobj = require("../db");
const Login= require('../model/login');
router.post("/signup",async (req,res)=>{

    const solt =  await bcrypt.genSalt(10);
    const pass = await bcrypt.hash(req.body.password,solt);

    var obj = {
        name: req.body.name,
        email: req.body.email,
        password: pass

    }
    //dbobj.signup(obj,res);
    await Login.create(obj);
    res.send({msg:"Registration done successfully"});
})
router.post("/login",async (req,res)=>{

   //dbobj.login(req.body.email, req.body.password,res)
  var result =  await Login.find({email:req.body.email});
  if(result.length>0)
  {
      console.log(req.body.password);
      console.log(result[0].password);

      bcrypt.compare(req.body.password,result[0].password,(err,finalres)=>{
          if(err){
              throw err;
          }
          else
          {
              if(finalres==true)
              {
                  var userDetails={
                      id:result[0]._id,
                      name:result[0].name,
                      email:result[0].email
                    }

                  var token = jwt.sign(userDetails,'moushree');
                  res.json({token:token});
              }
              else{
                  res.json({msg:"invalis user"})
              }
          }
      });
     
  }
  else
  {
    res.json({msg:"invalis user"});
  }
})

router.get('/protected',protectMiddleware, (req,res)=>{

    jwt.verify(req.jtoken, 'moushree',(err,data)=>{
        if(err)
        {
            res.json({msg:'Token Missmatch'})
        }
        else{
            res.json(data);
        }
    })
})
function protectMiddleware(req,res,next){
    const token = req.headers.authorization;
    if(typeof token != 'undefined'){

        const jtarr = token.split(' ');
        const jt = jtarr[1];
        req.jtoken = jt;
        next();
        
    }
    else{
        res.json({msg:'Token Missmatch'})
    }
 
}

module.exports = router;