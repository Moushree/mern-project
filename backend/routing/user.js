const express = require("express");

const router = express.Router();
//var dbobj = require("../db");
const Mou= require('../model/mou');
router.get("/sel",async(req,res)=>{
   //dbobj.sel_user(res);

   var result=await Mou.find();

   res.json(result);

})

router.post("/ins",async(req,res)=>{

var img = req.files.pimg
var imgname = Math.floor(Math.random()*1000)+img.name;
img.mv("./public/upimg/"+imgname,async(err)=>{
            if(err){
                throw err;
            }
            else{

                var insobj={
                    name:req.body.name,
                    email:req.body.email,
                    gender:req.body.gender,
                    country:req.body.country,
                    stream:req.body.stream,
                    pimg:imgname
    
                }
            
               // dbobj.ins_user(insobj,res);
                await Mou.create(insobj);
                res.json({msg:"Record inserted"});

            }
        }

)
  
})

router.post("/edit",async (req,res)=>{
    // dbobj.edit_user(req.body.id,res)
    var result=await Mou.findById(req.body.id);
    res.json(result);
})

router.post("/del",async(req,res)=>{
   //id = req.body.id;
  //dbobj.del_user(id,res);

  await Mou.findByIdAndRemove(req.body.id);
  res.json({msg:"Record Deleted"})

})

router.post("/upd",async (req,res)=>{

    if(req.files!=null){
    var img = req.files.pimg
    var imgname = Math.floor(Math.random()*1000)+img.name;
    img.mv("./public/upimg/"+imgname,async (err)=>{
                if(err){
                    throw err;
                }
                else{
    
                    var insobj={
                        name:req.body.name,
                        email:req.body.email,
                        gender:req.body.gender,
                        country:req.body.country,
                        stream:req.body.stream,
                        pimg:imgname
        
                    }
                
                    ////dbobj.upd_user(req.body.id,insobj,res);
                   await Mou.findOneAndUpdate({_id:req.body.id},{$set:insobj});
                    res.send({msg:"Record updated successfully"});
    
                }
            }
    
    )
        }else{
            
            var insobj={
                name:req.body.name,
                email:req.body.email,
                gender:req.body.gender,
                country:req.body.country,
                stream:req.body.stream,
                

            }
        
            // dbobj.upd_user(req.body.id,insobj,res);
            await Mou.findOneAndUpdate({_id:req.body.id},{$set:insobj});
            res.send({msg:"Record updated successfully"});
        }
})
module.exports = router;