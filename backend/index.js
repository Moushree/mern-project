const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const expressFileUpload = require("express-fileupload");
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
app.use(cors());

mongoose.connect('mmongodb+srv://moushree:m123456@cluster0.qbksq.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});
app.use(expressFileUpload());
app.use(express.static('public'));

app.get("/",(req,res)=>{
    res.send("Hello");
})


const pro = require("./routing/product");
const user = require("./routing/user");
const auth = require("./routing/auth");
app.use("/product",pro);
app.use("/user",user);
app.use("/auth",auth);

app.listen(2000);