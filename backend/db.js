var MongoClient = require('mongodb').MongoClient
const bcrypt = require('bcrypt');
const mongo = require('mongodb');
MongoClient.connect('mongodb+srv://moushree:m123456@cluster0.qbksq.mongodb.net/mern?retryWrites=true&w=majority',{ useUnifiedTopology: true }, function (err, client) {
  if (err) throw err

  var db = client.db('mern');

  const fs = require('fs');

  exports.ins_user=(insert,res)=>{

    db.collection("users").insertOne(insert,(err,suc)=>{

        if(err){
            throw err;
        }
        else{
            var out={msg:"Record Insert"}
            res.json(out);
        }
    })
}

// function create for selection

exports.sel_user = (res)=>{
    db.collection("users").find().toArray((err,result)=>{
        if(err){
            throw err;
        }
        else{

            res.json(result);
        }
    })
}

exports.del_user = (id,res)=>{

    var cond = {_id:new mongo.ObjectId(id)};

    db.collection("users").findOne(cond,(err,suc)=>{
        if(err){
            throw err;
        }
        else{
           fs.unlinkSync("./public/upimg/"+suc.pimg)
        }
    })

    db.collection("users").deleteOne(cond,(err,suc)=>{
        if(err){
            throw err;
        }
        else{
            res.json({msg:"Record Deleted"})
        }
    })
}

exports.edit_user = (id, res)=>{

    var cond = {_id:new mongo.ObjectId(id)};

    db.collection("users").findOne(cond,(err,result)=>{
        if(err){
            throw err;
        }
        else{
            res.json(result);
        }
    })

}

exports.upd_user=(id,val,res)=>{
    var cond = {_id:new mongo.ObjectId(id)};

       if(val.pimg!=null){

        db.collection("users").findOne(cond,(err,suc)=>{
            if(err){
                throw err;
            }
            else{
               fs.unlinkSync("./public/upimg/"+suc.pimg)
            }
        })

       }

    var newval={$set:val};
    db.collection("users").updateOne(cond,newval,(err,suc)=>{
        if(err){
            throw err;
        }
        else{
            res.json({msg:'Record Updated'})
        }
    })
}

exports.signup= (obj,res)=>{
    db.collection("login").insertOne(obj,(err,suc)=>{
        if(err){
            throw err;
        }
        else{
            res.json({msg:"Registration done"});
        }
    })
}
exports.login= (e,p,res)=>{
    db.collection("login").find({email:e}).toArray((err,result)=>{
        if(err){
            throw err;
        }
        else{
            if(result.length>0){
                bcrypt.compare(p,result[0].pass,(err,finalres)=>{
                    if(err){
                        throw err
                    }
                    else{
                        if(finalres==true){
                            res.json(result[0]);
                        }
                        else{
                            res.json({msg:"invalid login"})
                        }
                    }
                })
            }
            else{
                res.json({msg:"invalid login"});
            }
        }
    })
}


})